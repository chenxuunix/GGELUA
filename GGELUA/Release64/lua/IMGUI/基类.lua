--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 17:08:13
    @LastEditTime : 2021-05-08 01:35:51
--]]
local im = require"gimgui"

local IMBase = class('IMBase')

function IMBase:IMBase()

end

function IMBase:更新()
    if self._bwidth then
        im.SetNextItemWidth(self._bwidth)
    end
end

function IMBase:置宽度(w)
    self._bwidth = w
    return self
end

function IMBase:不换行()
    im.SameLine()
    return self
end

function IMBase:文本(t,...)
    if select("#", ...)>0 then
        t = t:format(...)
    end
    im.TextUnformatted(t)
end

function IMBase:是否停留()
    return im.IsItemHovered()
end

function IMBase:提示(t)
    return im.SetTooltip(t)
end

function IMBase:是否选中()
    return self[1]
end

function IMBase:取值()
    return self[1]
end
return IMBase